library(randomForest)
library(ROCR)
library(glmnet)
library(gbm)
library(xgboost)
library(caret)


################################################################################
# Data preprocessing----

train_data <- function(files, train=NULL, test=NULL){
  #
  # Args: 
  #   files : Required, as a list of text files to work over.
  #   train : Optional, as the csv file name for the training labels.
  #   test  : Optional, is used to see if all html docs have either a 
  #           training_label or a testing id. During development, 
  #           some docs did not have either.
  #
  # Returns:
  #   4 Options:
  #     1:train = NULL
  #         Data.frame of unique observations in files. No labels attached.
  #         And no comparison against train/test for missing files
  #     2:test = NULL
  #         Data.frame of unique observations in files, training labels attached.
  #         No comparison against test for missing files
  #     3:test and train != NULL, all entries in files have training labels
  #         Same data.frame as in option 2. No need for comparisons.
  #     4:test and train != NULL, some entries in files do not have training labels
  #         List of 2 elements:
  #           1st is the data.frame as in option 2, 
  #           2nd is a data.frame of entries with missing train/test labels.
  # 
  # TODO:
  #   - [] Update return option 2 to check for missing training labels and 
  #         return file names.
  # 
  
  if (!length(files)>0) stop("No files passed to function.")
  
  if(length(files)>1){ # Loop through files
    # Load files into a list
    parsed <- list()
    for(i in 1:length(files)){
      parsed_data <- read.csv(paste0("./data/",files[i]), header=T, row.names = 1)
      parsed[[i]] <- parsed_data
    }
    
    # Now to merge them together. First element of parsed is loaded into data,
    # and the rest are rbind after removing any duplicates.
    data_temp <- parsed[[1]]
    for(i in 2:length(parsed)){
      dup_rows <- row.names(parsed[[i]]) %in% row.names(data_temp)
      data_temp <- rbind(data_temp,parsed[[i]][!dup_rows,])
    }
  } else { # Only one data file to be parsed.
    parsed_data <- read.csv(paste0("./data/",files), header=T, row.names = 1)
    data_temp <- parsed_data
  }
  
  if (is.null(train)) {print("train is null"); return(data_temp)}
  
  # Now to attach the training labels
  
  train_labels <- read.csv(paste0("./data/", train), header=T, row.names=1)
  
  # Attach labels to data. 
  # Merge will look for rownames that match between the 2 dataframes
  # and then join the 2 together. In our case, trainLabels only has 1 column, 
  # so it is essentially a cbind when (row.name(data)==row.name(train)) = True
  
  data_temp2 <- merge(data_temp, train_labels, by="row.names")
  row.names(data_temp2) <- data_temp2[,1] # Reattach row names
  data_temp2 <- data_temp2[,-1] # Drop extraneous column
  data_temp2$sponsored <- as.factor(data_temp2$sponsored)
  
  
  if (is.null(test)) {print("test is null"); return(data_temp2)}
  
  # During testing, noticed that data_temp2 is shorter than data_temp, 
  # indicating that some files did not have training labels.
  # The following retrieves those files which have no training label and returns them.
  
  test_file <- read.csv(paste0("./data/", test), header=T, row.names=1)
  
  missing_index <- which(row.names(data_temp) %in% row.names(data_temp2) == FALSE)
  missing_filename <- matrix(data=TRUE,nrow=length(missing_index), ncol=3)
  colnames(missing_filename) <- c("File", "Missing Training", "Missing Test")
  missing_filename[,1] <- row.names(data_temp[missing_index,])
  
  # Additionally, the following will check to see if the files instead have a testing id.
  missing_filename[,3] <- !(missing_filename[,1] %in% row.names(test_file))
  
  if(length(missing_index) != 0) {
    warning("There are files with missing labels. Your results are now in a list:\n1st element being data,\n2nd a list of files which have missing training or test labels.")
    return(list(data_temp2,missing_filename))
  }
  
  return(data_temp2)
}
test_data <- function(test_file, submission_csv=NULL){
  #
  # Args: 
  #   test_file       : Required, raw data.
  #   submission_csv  : Required, some raw data are to be ignored, 
  #                     sample_submission_csv is the master reference.
  #
  # Returns:
  #   data.frame of files which exist in submission_csv
  # 
  # TODO:
  # 
  
  if (!length(test_file)>0) stop("No data passed to function.")
  if (is.null(submission_csv)) stop("Need submission_csv for cross reference.")
  
  data_temp <- read.csv(paste0("./data/",test_file), header=T, row.names = 1)
  cross_ref <- read.csv(paste0("./data/",submission_csv), header=T, row.names=1)
  
  missing_index <- (row.names(data_temp) %in% row.names(cross_ref))
  data_temp2 <- data_temp[-missing_index,]
  
  print(paste0("There are ",nrow(data_temp2)," files for prediction."))
 
  return(data_temp2)
}

train_cross_ref <- "train_v2.csv" # Name of file with training labels. Do not need directory.
test_cross_ref <- "sampleSubmission_v2.csv" # Name of file with test IDs. Do not need directory.

training_files <- c("multiprocessed_parsed_text_output_ALL01234DATADIRS__337277_files.txt")
testing_files <- c("multiprocessed_parsed_text_output_TEST_DIR05__66767_files.txt")

test <- test_data(testing_files, test_cross_ref)
data <- train_data(training_files, train_cross_ref, test_cross_ref)

missing_labels <- data[[2]]
data <- data[[1]]
y_column <- match("sponsored", names(data))

# Split into train/validate 85/15%
set.seed(2015)
train_split <- rep(TRUE, nrow(data)) # vector TRUE = training set
validate <- sample.int(nrow(data), floor(0.15*nrow(data)))
train_split[validate] <- FALSE # Replace ~15% of the true with false, and they will become validation

################################################################################
# Random Forest----

kaggle_rf_default <- randomForest(x=data[train_split,-y_column], y=data[train_split,y_column], importance = T)
print(kaggle_rf_default)

pred_rf_default_1 <- predict(kaggle_rf_default, data[!train_split,-y_column], type="prob")[,2]
pred_rf_default_2 <- prediction(pred_rf_default_1, data[!train_split,y_column])
perf_rf_default_1 <- performance(pred_rf_default_2,"tpr","fpr")
auc.temp <- performance(pred_rf_default_2,"auc"); auc <- as.numeric(auc.temp@y.values)
plot(perf_rf_default_1,main=paste0("ROC Curve for Random Forest\n AUC=",round(auc,4)),col=2,lwd=2)
abline(a=0,b=1,lwd=2,lty=2,col="gray")

importance(kaggle_rf_default)
varImpPlot(kaggle_rf_default)
gc()

# Following may be useful to highlight important variables for the class presentation.
# 
# importance_matrix <- importance(kaggle_rf_default)
# importance_matrix <- importance_matrix[order(importance_matrix[,3], 
#                                              decreasing = T),]  
# plot(sort(round(importance(kaggle_rf_default)[,3], 2), decreasing=T), 
#      main="Variable importance plot for Spam/Ham dataset", 
#      ylab= "Mean decrease in Accuracy")
# plot(kaggle_rf_default$err.rate[,1], main="OOB error rate over 500 trees", 
#      type = "l", ylab="OOB error", xlab="# tree", sub = "m=7, nmin=1")
# partialPlot(kaggle_rf_default, pred.data=data[!train_split,-i], 
#             x.var="Number of <a> (anchor) tag links found", which.class = "1",
#             main="Partial Dependence on \'!\' for class=\'Spam\'",
#             xlab="Character Freq '!'")

################################################################################
# Logistic Regression----

# Switch on parallel if desired. Change makeCluster() to however many cores you want.
library(doParallel)
cl <- makeCluster(6)
registerDoParallel(cl)


x_train <- as.matrix(data[train_split,-y_column])
y_train <- as.factor(data[train_split,y_column])
x_test <- as.matrix(data[!train_split,-y_column])

# Parallel
kaggle_logistic_default <- cv.glmnet(x=x_train, y=y_train, 
                                     family="binomial", nfolds=10, 
                                     type.measure="auc", parallel = T)
# Single core
# kaggle_logistic_default <- cv.glmnet(x=x_train, y=y_train, 
#                                      family="binomial", nfolds=10, 
#                                      type.measure="auc", parallel = F)

pred_logistic_default_1 <- predict(kaggle_logistic_default, x_test, 
                             s=c("lambda.1se"), type="response")
pred_logistic_default_2 <- prediction(pred_logistic_default_1, data[!train_split,y_column])
perf_logistic_default_1 <- performance(pred_rf_default_2,"tpr","fpr")
auc.temp <- performance(pred_logistic_default_2,"auc"); auc <- as.numeric(auc.temp@y.values)
plot(perf_logistic_default_1,main=paste0("ROC Curve for Logistic Regression\n AUC=",round(auc,4)),col=2,lwd=2)
abline(a=0,b=1,lwd=2,lty=2,col="gray")

stopCluster(cl)

################################################################################
# GBM - !Not working, crashes r session??!!!!!----
numbertrees = 30000
cvfolds = 5

m1 <- gbm(formula = sponsored~.,
          distribution="bernoulli",
          shrinkage = 0.001,
          n.trees = numbertrees,
          cv.folds = cvfolds,
          bag.fraction = 0.5,          
          train.fraction = 0.5, 
          interaction.depth=6,
          data=data,
          verbose=FALSE,
          n.cores=6)

################################################################################
# XGBoost. Not working yet.----
xgb_train <- as.matrix(sapply(data[train_split,-y_column], as.numeric))
# xgb_label <- as.matrix(sapply(data[train_split,-y_column], as.numeric))
xgb_label <- as.numeric(levels(data[train_split,y_column])[data[train_split,y_column]])
boost_matrix <- xgb.DMatrix(xgb_train, label=xgb_label); rm(xgb_train); rm(xgb_label); gc()

kaggle_xgb_default <- xgboost(data=boost_matrix, max.depth=2, eta=1, nthread=4,
                              nround=10, objective="binary:logistic")
kaggle_xgb_cv <- xgb.cv(data = boost_matrix, nround=100, nthread = 6, nfold = 5, metrics="auc",
                  max.depth =5, eta = 1, objective = "binary:logistic")


cv.ctrl <- trainControl(method = "repeatedcv", repeats = 1,number = 3, 
                        #summaryFunction = twoClassSummary,
                        classProbs = TRUE,
                        allowParallel=T)

xgb.grid <- expand.grid(nrounds = 1000,
                        eta = c(0.01,0.05,0.1),
                        max_depth = c(2,4,6,8,10,14)
)
set.seed(45)
xgb_tune <-train(
                 data=boost_matrix,
                 method="xgbTree",
                 trControl=cv.ctrl,
                 tuneGrid=xgb.grid,
                 verbose=T,
                 metric="Kappa",
                 nthread =3
)

################################################################################
# Mini ensemble.----


wt <- c(1,1)/2 # (RF:Logistic)/total . Used to weight average

# Combine results
ensem <- cbind(pred_rf_default_1, pred_logistic_default_1) 

# take the mean of the results
pred_ensem <- apply(ensem, 1, function(x) weighted.mean(x[1:2], wt)) 

pred_ensem_2 <- prediction(pred_ensem, data[!train_split,y_column])
perf_ensem_1 <- performance(pred_ensem_2,"tpr","fpr")
auc.temp <- performance(pred_ensem_2,"auc"); auc <- as.numeric(auc.temp@y.values)
plot(perf_ensem_1,main=paste0("ROC Curve for Ensemble\n AUC=",round(auc,4)),col=2,lwd=2)
abline(a=0,b=1,lwd=2,lty=2,col="gray")
cor(ensem)

################################################################################
# Simple RF used to create predictions for initial kaggle submission.----

system.time({kaggle_rf_default <- randomForest(x=data[,-y_column], y=data[,y_column], 
                                               importance = F); gc()})/60
# Uses about 11gb RAM, and takes 14 minutes.
print(kaggle_rf_default)

pred_rf_default_kaggle <- as.matrix(predict(kaggle_rf_default, test, type="prob")[,2])

# The following is a dogs breakfast, but will serve as atemplate for future submissions.
# There are some virus files that were not parsed and so the following code 
# figures out which are missing and assigns uniform(0,1) and then updates column 
# names etc and writes a submission file.
i <- match(rownames(pred_rf_default_kaggle), rownames(cross_ref))
cross_ref[i,1] <- pred_rf_default_kaggle[,1] # Change according to which model is being used.
cross_ref[-i,1] <- runif(n=length(cross_ref[-i,1]))
cross_ref[,2] <- cross_ref[,1]
cross_ref[,1] <- row.names(cross_ref)
row.names(cross_ref) <- NULL
colnames(cross_ref) <- c("file","sponsored")
write.csv(cross_ref, "Submission1.csv", row.names=F)
